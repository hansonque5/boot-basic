FROM java:8-jdk-alpine
COPY ./target/boot-basic-0.0.1-SNAPSHOT.jar /usr/app/
WORKDIR /usr/app
ENTRYPOINT ["java", "-jar", "boot-basic-0.0.1-SNAPSHOT.jar"]