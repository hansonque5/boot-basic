package com.example.boot;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {
@GetMapping("/{name}")
 public String welcome(@PathVariable String name) {
  return "Hello, "+name + "!  Welcome to the website! ";
 }
}